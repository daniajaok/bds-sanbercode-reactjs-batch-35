// di file callback.js
function readBooks(time, book, callback ) {
    console.log("saya membaca " + book.name )
    setTimeout(function(){
        let sisaWaktu = 0
        if(time >= book.timeSpent) {
            sisaWaktu = time - book.timeSpent
            console.log("saya sudah membaca " + book.name + ", sisa waktu saya " + sisaWaktu)
            callback(sisaWaktu) //menjalankan function callback
        } else {
            console.log('waktu saya habis')
            callback(time)
        }   
    }, book.timeSpent)
}
var books = [
    {name: 'LOTR', timeSpent: 3000}, 
    {name: 'Fidas', timeSpent: 2000}, 
    {name: 'Kalkulus', timeSpent: 4000},
    {name: 'komik', timeSpent: 1000}
]
 
// Tulis code untuk memanggil function readBooks di sini ( menggunkan callback )
//readBooks(books[0].timeSpent,books[0].name,function(paraasas){console.log(paraasas)})
//console.log(books[0].timeSpent)
function baca (time,books,i){
    if(i<books.length){
        readBooks(time,books[i],function(waktu){
            if (waktu>0){
                i+=1;
                baca(waktu,books,i)
            }
        })
    }
}
baca(10000,books,0)
/*
books.forEach(element => readBooks(10000, element, (callbackFn) => {
    console.log(callbackFn)
  }))
Books adalah array of objects. Kita bisa iterate array tersebut menggunakan map/forEach. Map akan me-return sebuah array baru, kita tidak butuh array baru, makanya kita hanya perlu menggunakan forEach.

Intinya, code tersebut:
1. forEach akan iterate semua element di dalam array.
2. Di setiap element, kita akan panggil Fn readBooks.
3. Parameter pertama kita masukan waktu, di case saya saya masukan 10s. Kedua, kita masukan element yang sedang aktif. Ketiga saya membuat callbackFn yang berfungsi untuk console.log.

*/