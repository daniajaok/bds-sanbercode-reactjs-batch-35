// di file promise.js
function readBooksPromise (time, book) {
    console.log("saya mulai membaca " + book.name )
    return new Promise( function (resolve, reject){
      setTimeout(function(){
        let sisaWaktu = time - book.timeSpent
        if(sisaWaktu >= 0 ){
            console.log("saya sudah selesai membaca " + book.name + ", sisa waktu saya " + sisaWaktu)
            resolve(sisaWaktu)
        } else {
            console.log("saya sudah tidak punya waktu untuk baca "+ book.name)
            reject(sisaWaktu)
        }
      }, book.timeSpent)
    })
  }

  var books = [
    {name: 'LOTR', timeSpent: 3000}, 
    {name: 'Fidas', timeSpent: 2000}, 
    {name: 'Kalkulus', timeSpent: 4000}
]
 
// Lanjutkan code untuk menjalankan function readBooksPromise ( menggunakan promise )
function startReading(remainingTime, books, index) {
    readBooksPromise(remainingTime, books[index], function(time) {
        const nextBook = index + 1;
        if (nextBook < books.length) {
            startReading(time, books, nextBook);
        }
    });
}

startReading(10000, books, 0);
/*
Disitu ada fungsi baru dengan nama startReading yang akan menerima parameter sisa waktu, daftar buku, beserta indexnya.
Lalu dari situ bisa kita panggil fungsi readBooksPromise yang akan mengembalikan nilai time ketika callback, lalu time tersebut bisa dimanfaatkan untuk memanggil fungsi startReading lagi dengan index buku yang selanjutnya.
Sebelum memanggil fungsi startReading lagi, dilakukan pengecekan apakah nextBook tidak lebih dari index buku, jika true, maka dipanggil, jika tidak maka fungsi akan selesai
 */ 