//tugas 1
const luasPersegiPanjang= (p1, p2) => {
    return p1 * p2;   
}
const kelilingPersegiPanjang = (p1, p2) => {
    return p1 * p2;   
}
const volumeBalok= (p1, p2,p3) => {
    return p1 * p2*p3;   
}
let panjang= 12
let lebar= 4
let tinggi = 8
let HasilluasPersegiPanjang = luasPersegiPanjang(panjang, lebar)
let HasilkelilingPersegiPanjang = kelilingPersegiPanjang(panjang, lebar)
let HasilvolumeBalok = volumeBalok(panjang, lebar, tinggi)

console.log(HasilluasPersegiPanjang ) 
console.log(HasilkelilingPersegiPanjang )
console.log(HasilvolumeBalok )
//tugas 2
/* 
    Tulis kode function di sini
*/
const introduce= (p1, p2,p3,p4) => {
    return "pak "+p1+" adalah seseorang "+p4+" yang berusia"+p2; 
}

//kode di bawah ini jangan dirubah atau dihapus
const perkenalan = introduce("John", "30", "Laki-Laki", "penulis")
console.log(perkenalan) // Menampilkan "Pak John adalah seorang penulis yang berusia 30 tahun"
//tugas 3
let arrayDaftarPeserta = ["John Doe", "laki-laki", "baca buku" , 1992]
let objDaftarPeserta = {    
    Nama : "John Doe",
    JenisKelamin: "laki-laki",
    Hoby: "baca buku",
    TahunLahir: 1992
}
console.log(objDaftarPeserta)
//tugas 4
var buah = [
    {Nama: "nanas", warna: "kuning", adaBijinya: false,harga:9000}, 
    {Nama: "jeruk", warna: "Oranye", adaBijinya: true,harga:8000}, 
    {Nama: "semangka", warna: "hijau & merah", adaBijinya: true,harga:10000},
    {Nama: "pisang", warna: "kuning", adaBijinya: false,harga:5000}
    ]
console.log(buah[0],buah[3]) 
//tugas 5
let phone = {
    name: "Galaxy Note 20",
    brand: "Samsung",
    year: 2020,
    colors: ["Mystic Bronze", "Mystic White", "Mystic Black"]
 }
 // kode diatas ini jangan di rubah atau di hapus sama sekali
 
 /* Tulis kode jawabannya di sini */
 // kode di bawah ini jangan dirubah atau dihapus
 const phoneBrand = phone.brand;
 const phoneName = phone.name;
 const year = phone.year;
 const colorBlack = phone.colors[2];
 const colorBronze = phone.colors[0];
 console.log(phoneBrand, phoneName, year, colorBlack, colorBronze) 
//tugas 6
let warna = ["biru", "merah", "kuning" , "hijau"]

let dataBukuTambahan= {
  penulis: "john doe",
  tahunTerbit: 2020 
}

let buku = {
  nama: "pemograman dasar",
  jumlahHalaman: 172,
  warnaSampul:["hitam"]
}
// kode diatas ini jangan di rubah atau di hapus sama sekali

/* Tulis kode jawabannya di sini */ 
let newperson={...buku,warna,...dataBukuTambahan}
console.log(newperson)
// tugas 7
const tambahDataFilm= (p1, p2,p3,p4) => {
var dataFilm = [
    {Nama: p1, durasi: p2, genre: p3,action:p4}

    ]
console.log(dataFilm)
}
tambahDataFilm("LOTR", "2 jam", "action", "1999")
tambahDataFilm("avenger", "2 jam", "action", "2019")
tambahDataFilm("spiderman", "2 jam", "action", "2004")
tambahDataFilm("juon", "2 jam", "horror", "2004")
