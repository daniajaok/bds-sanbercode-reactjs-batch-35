import React from 'react';
import './style.css';
import { BrowserRouter as Router,Switch,Route,Link  } from 'react-router-dom';

const nav=(props)=> {
    return (
        <>
        <nav className='nav'>
            <ul>
                <li className='li li'><Link to="/Tugas10">tugas10</Link></li>
	 	        <li className='li li'><Link to="/Tugas11">Tugas11</Link></li>
                <li className='li li'><Link to="/Tugas12">Tugas12</Link></li>
                <li className='li li'><Link to="/Tugas13">Tugas13</Link></li>
                <li className='li li'><Link to="/Tugas14">Tugas14</Link></li>
            </ul>
        </nav>
        </>
    );
}

export default nav;