//soal ke 1 
let word = 'JavaScript'; 
let second = 'is'; 
let third = 'awesome'; 
let fourth = 'and'; 
let fifth = 'I'; 
let sixth = 'love'; 
let seventh = 'it!';
//jawaban no 1
console.log(word+" "+second+" "+third+" "+fourth+" "+fifth+" "+sixth+" "+seventh);

//soal ke 2
let kataPertama = "saya"; 
let kataKedua = "senang"; 
let kataKetiga = "belajar"; 
let kataKeempat = "javascript";
//jawaban ke 2
console.log(kataPertama+" "+kataKedua+" "+kataKetiga+" "+kataKeempat);

//soal ke 3 
let panjangPersegiPanjang = "8";
let lebarPersegiPanjang = "5";

let alasSegitiga= "6";
let tinggiSegitiga = "7";

let kelilingPersegiPanjang = lebarPersegiPanjang * lebarPersegiPanjang;
let luasSegitiga =0.5* alasSegitiga* tinggiSegitiga;
//jawaban ke 3
console.log(" hasil dari keliling Persegi Panjang  adalah : "+kelilingPersegiPanjang);
console.log(" hasil dari luas Segitiga  adalah : "+luasSegitiga);

//soal ke 4
let sentences= 'wah javascript itu keren sekali'; 

let firstWords= sentences.substring(0, 3); 
let secondWords= sentences.substring(4, 14); 
let thirdWords= sentences.substring(15, 19); 
let fourthWords= sentences.substring(19, 25); 
let fifthWords= sentences.substring(25, 32); 
//jawaban ke 4
console.log('Kata Pertama: ' + firstWords); 
console.log('Kata Kedua: ' + secondWords); 
console.log('Kata Ketiga: ' + thirdWords); 
console.log('Kata Keempat: ' + fourthWords); 
console.log('Kata Kelima: ' + fifthWords);

//soal ke 5
var sentence = "I am going to be React JS Developer"; 

var exampleFirstWord = sentence[0] ; 
var exampleSecondWord = sentence[2] + sentence[3]  ; 
var thirdWord= sentence[4] + sentence[5]+ sentence[6]+ sentence[7]+ sentence[8]+ sentence[9]+ sentence[10]; // lakukan sendiri, wajib mengikuti seperti contoh diatas 
var fourthWord= sentence[11] + sentence[12]; // lakukan sendiri , wajib mengikuti seperti contoh diatas
var fifthWord= sentence[14] + sentence[15]; // lakukan sendiri , wajib mengikuti seperti contoh diatas
var sixthWord= sentence[17] + sentence[18]+ sentence[19]+ sentence[20]+ sentence[21]; // lakukan sendiri , wajib mengikuti seperti contoh diatas
var seventhWord= sentence[23] + sentence[24]; // lakukan sendiri , wajib mengikuti seperti contoh diatas
var eighthWord= sentence[26] + sentence[27]+ sentence[28]+ sentence[29]+ sentence[30]+ sentence[31]+ sentence[32]+ sentence[33]+ sentence[34]; // lakukan sendiri , wajib mengikuti seperti contoh diatas
//jawaban ke 5
console.log('First Word: ' + exampleFirstWord); 
console.log('Second Word: ' + exampleSecondWord); 
console.log('Third Word: ' + thirdWord); 
console.log('Fourth Word: ' + fourthWord); 
console.log('Fifth Word: ' + fifthWord); 
console.log('Sixth Word: ' + sixthWord); 
console.log('Seventh Word: ' + seventhWord); 
console.log('Eighth Word: ' + eighthWord);

//soal ke 6
let txt = "I can eat bananas all day";
let hasil=txt.slice(10,18); //lakukan pengambilan kalimat di variable ini


//jawaban ke 6
console.log(hasil);