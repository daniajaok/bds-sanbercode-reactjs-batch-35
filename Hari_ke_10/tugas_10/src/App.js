import logo from './logo.svg';
import './App.css';
import logo2 from './assets/img/logo.png';


const ListCard=(props)=>{
  return(
  
    <>
    <div className="list.card">
    <input type={`checkbox`}/>{props.text} 
    </div>
    <hr/>

    </>
  )
}

function App() {
  return (
    <>
    <div className="card">
    <img src={`${logo2}`}/>
    <h1>THINGS TO DO</h1>
    <small>During bootcamp in sambercode</small>
    <hr/>
    <ListCard text="Belajar Git & cli "/>
    <ListCard text="Belajar HRML & css "/>
    <ListCard text="Belajar javascipts"/>
    <ListCard text="Belajer Reactjs Dasar  "/>
    <ListCard text="Belajer Reactjs Advance "/>
    
   <a className="a" href="">SEND</a>

    </div>
    </>
  );
}

/* cara ribet
    <hr/>
    <div className="list.card">
    <input type={`checkbox`}/>Belajar HRML & css
    </div>
    <hr/>
    <div className="list.card">
    <input type={`checkbox`}/>Belajar javascipts
    </div>
    <hr/>
    <div className="list.card">
    <input type={`checkbox`}/>Belajer Reactjs Dasar 
    </div>
    <hr/>
    <div className="list.card">
    <input type={`checkbox`}/>Belajer Reactjs Advance
    </div>
 */

export default App;
