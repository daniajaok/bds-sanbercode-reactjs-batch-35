import logo2 from '../assets/img/logo.png';
import '../assets/css/style.css';
import React, { useEffect, useState } from 'react';
import axios from 'axios';

const Index=(props) =>{
 /* 
  const [dataMahasiswa,setDataMahasiswa] kosep geter  dan seter, dataMahasiswa itu getter sedangakn yg kanannya setter.
  merubah value di state tersebut, kita panggilnya yg sebelah kanan
  */
  const [dataMahasiswa,setDataMahasiswa]=useState([])
  const [fetchStatus,setFetchStatus]=useState(true)//tips agara lebih mudah
  const [inputName, setInputName] =  useState("")//assign value 
  const [currentId, setCurrentId] =  useState(null)

  useEffect(()=>{
    let fetchData= async()=>{
        let {data} = await axios.get(`https://backendexample.sanbercloud.com/api/mobile-apps`)//kita akan mengunakan Destructuring adalah memecah komponen yang kompleks menjadi komponen yang lebih kecil dan mengunakan async promise
        //console.log(data)
        //setDataMahasiswa([...data])
        let result = data.map((res)=>{  
            //https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/map
            
            let {
                id,
                name,
                category,
                description,
                release_year,
                size,
                price,
                rating,
                image_url,
                is_ios_app,


            }=res//kita akan mengunakan Destructuring adalah memecah komponen yang kompleks menjadi komponen yang lebih kecil
            return{ 
              id,               
              name,
              category,
              description,
              release_year,
              size,
              price,
              rating,
              image_url,
              is_ios_app,}

            
        })
        setDataMahasiswa([...result])

    }
    if(fetchStatus){//trik if 
        fetchData()
        setFetchStatus(false)
    }
    
    
  },[fetchStatus,setFetchStatus])//dependensi https://www.youtube.com/watch?v=uXsVSorOtAY 1:16:25 / 1:38:12


  const handleIndexScore=(prams)=>{
    if(prams>=1){
        return "android"
    }
    else {
        
        return "iphone"
    }
    

  }
  const handleIRD=(prams)=>{

    if(prams<=1){
        return "Free"
    }
    else{
    const format = prams.toString().split('').reverse().join('');
    const convert = format.match(/\d{1,3}/g);
    const rupiah = 'Rp ' + convert.join('.').split('').reverse().join('')
    return(rupiah)
    }



  }
  const KbtoMb=(parameter)=>{
    const format= parameter /1000;
    return(format)
  }

    
    return (
        <div>
            <div>
                {/*             <div className="topnav">
                <a href>
                <img src={`${logo2}`} width={70} />
                </a>
                <a href="#">Home</a>
                <a href="#">Movie List</a>
                <a href="#">About</a>
                <form>
                <input type="text" />
                <input type="submit" defaultValue="Cari" />
                </form>
            </div>*/}


            {/* edit  di sini */}
            <div className="row">
                <div className="section">
                <div className="card">
                    {dataMahasiswa !== null&&(
                        <>
                            {
                                dataMahasiswa.map((res,index)=>{
                                    return(
                                        <div>
                                            
                                            <strong><h1>{res.name}</h1></strong>
                                            
                                            <h5>Release Year :{res.release_year}</h5>
                                            <img classname="fakeimg" style={{width: '50%', height: '300px', objectFit: 'cover'}} src={`${res.image_url}`}/>
                                            <br />
                                            <br />
                                            <div>
                                                <strong>price: {handleIRD(res.price)}</strong><br />
                                                <strong>rating:{res.rating} </strong><br />
                                                <strong>size:{KbtoMb(res.size)} MB </strong><br />
                                                <strong style={{marginRight: '10px'}}>Platform : {handleIndexScore( res.is_ios_app)}
                                                </strong>
                                                <br />
                                            </div>
                                            <p>
                                                <strong style={{marginRight: '10px'}}>Description :</strong>
                                                {res.description}
                                            </p>
                                            <hr />
                                        </div>
                                    )
                                })
                            }
                        </>
                    )}

                </div>
                </div>
            </div>
            <footer>
                <h5>© Quiz 3 ReactJS Sanbercode</h5>
            </footer>
            </div>

        </div>
    );
}

export default Index;