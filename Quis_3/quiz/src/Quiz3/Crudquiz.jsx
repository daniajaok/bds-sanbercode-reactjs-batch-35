import logo from '../logo.svg';
import '../App.css';
import React, { useEffect, useState } from 'react';
import '../assets/css/style.css';
import axios from 'axios';


const Crudquiz=()=> {

 /* 
  const [dataMahasiswa,setDataMahasiswa] kosep geter  dan seter, dataMahasiswa itu getter sedangakn yg kanannya setter.
  merubah value di state tersebut, kita panggilnya yg sebelah kanan
  */
  const [dataMahasiswa,setDataMahasiswa]=useState([])
  const [fetchStatus,setFetchStatus]=useState(true)//tips agara lebih mudah
  const [inputName, setInputName] =  useState("")//assign value 
  const [currentId, setCurrentId] =  useState(null)

  useEffect(()=>{
    let fetchData= async()=>{
        let {data} = await axios.get(`https://backendexample.sanbercloud.com/api/mobile-apps`)//kita akan mengunakan Destructuring adalah memecah komponen yang kompleks menjadi komponen yang lebih kecil dan mengunakan async promise
        //console.log(data)
        //setDataMahasiswa([...data])
        let result = data.map((res)=>{  
            //https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/map
            
            let {
                id,
                name,
                category,
                description,
                release_year,
                size,
                price,
                rating,
                image_url,
                is_ios_app,


            }=res//kita akan mengunakan Destructuring adalah memecah komponen yang kompleks menjadi komponen yang lebih kecil
            return{ 
              id,               
              name,
              category,
              description,
              release_year,
              size,
              price,
              rating,
              image_url,
              is_ios_app,}

            
        })
        setDataMahasiswa([...result])

    }
    if(fetchStatus){//trik if 
        fetchData()
        setFetchStatus(false)
    }
    
    
  },[fetchStatus,setFetchStatus])//dependensi https://www.youtube.com/watch?v=uXsVSorOtAY 1:16:25 / 1:38:12


  console.log(dataMahasiswa)
  const handleIndexScore=(prams)=>{
    if(prams>=1){
        return "android"
    }
    else {
        
        return "iphone"
    }
    

  }
  /* 
  const [input,setInput] kosep geter  dan seter, daftarBuah itu getter sedangakn yg kanannya setter.
  merubah value di state tersebut, kita panggilnya yg sebelah kanan
  */
  const [input,setInput]=useState({
    name:"",
    category:"",
    description:"",
    release_year:2000,
    size:0,
    price:0,
    rating:0,
    image_url:"",
    is_ios_app:0,
      //tempat penapung get maupun post atau  geter  dan seter default
      ////assign value 


  })


  const handleChange=(event)=>{//event adalah parameter 
    console.log(event.target)
    //event.terget berfungsi nemapilan atribut <input type="text"name="name" value>
    console.log(event.target.name)//event.target.name untuk memilih target spesifik
    let nameOfInput=event.target.name
    let valueOfInput=event.target.value
    console.log(valueOfInput)
    //cara menetapkan secara langsung ke dalam useState dengan cara 
    setInput({...input,[nameOfInput]:valueOfInput})
  }
  //console.log(setInput)
  console.log("hasil dari input "+input)
  console.log(fetchStatus)
  //
  const handleSubmit=(event)=>{//event adalah parameter
    event.preventDefault()
    console.log("hasil format"+input)//chek apakah sudah sesuai format array of oject dataMahasiswa
    let {name,category,description,release_year,size,price,rating,image_url,is_ios_app}=input//kita akan mengunakan Destructuring adalah memecah komponen yang kompleks menjadi komponen yang lebih kecil
    console.log("ini adalah nama "+name)
    console.log("ini adalah id"+currentId)

    if (currentId === null){
      // untuk create data baru
      axios.post(`https://backendexample.sanbercloud.com/api/mobile-apps`, {name,category,description,release_year,size,price,rating,image_url,is_ios_app})
      .then(res => {
        console.log(res)//untuk menagkap error 
          setFetchStatus(true)//1:11:56 / 1:38:12 https://www.youtube.com/watch?v=uXsVSorOtAY
      //    let data = res.data
      //    setPesertaLomba([...pesertaLomba, {id: data.id, name: data.name}])
     })
    }
    else{
      axios.put(`https://backendexample.sanbercloud.com/api/mobile-apps/${currentId}`, {name,category,description,release_year,size,price,rating,image_url,is_ios_app})
      .then((res) => {
        console.log(res)//untuk menagkap error 
        setFetchStatus(true)//1:11:56 / 1:38:12 https://www.youtube.com/watch?v=uXsVSorOtAY
          //let singlePeserta = currentId.find(el=> el.id === currentId)
          //singlePeserta.name= input.name
          //console.log("data nama"+singlePeserta.name)
          //setDataMahasiswa([...dataMahasiswa])
      })
    }



    setInput({
        
        name:"",
        category:"",
        description:"",
        release_year:2000,
        size:0,
        price:0,
        rating:0,
        image_url:"",
        is_ios_app:0,



    })


  }



  const handleDelete=(event)=>{
  let idData = parseInt(event.target.value)
  console.log("asdfasdasdsad"+idData)//cek apakah id udah terambil atau belum
  axios.delete(`https://backendexample.sanbercloud.com/api/mobile-apps/${idData}`)
  .then((res)=>{
    console.log(res)//untuk menagkap error 
    setFetchStatus(true)
    
  })
  .catch((err)=>{
    console.log(err)//untuk menagkap error

})

  }



  const handleEdit=(event)=>{
    let idData = parseInt(event.target.value)
    console.log("tampilkan edit ID"+idData)//cek apakah id udah terambil atau belum
    axios.get(`https://backendexample.sanbercloud.com/api/mobile-apps/${idData}`)
    .then(res => {
      let data = res.data

      setInput(data)//=>araay of object diamasukan setInput diluar fungsi
      //setDataMahasiswa(data.name,data.course,data.score)
      console.log(data)
      console.log("tampilkan edit semua data "+data.name,data.course,data.score)
      setCurrentId(data.id)
      console.log("tampilkan edit id "+data.id)
    })
  }


  

  return (
    <>





            {/**dsdsdsdsdsd */}
            <div className="row">
              <div className="section">
                <div className="card">
                  <div className="relative overflow-x-auto shadow-md sm:rounded-lg">
                    <table className="w-full text-sm text-left text-gray-500 dark:text-gray-400">
                      <thead className="text-xs text-gray-700 uppercase bg-gray-50 dark:tex-dark-400">
                        <tr>
                          <th scope="col" className="px-6 py-3">
                          No
                          </th>
                          <th scope="col" className="px-6 py-3">
                          Nama
                          </th>
                          <th scope="col" className="px-6 py-3">
                          Category
                          </th>
                          <th scope="col" className="px-6 py-3">
                          Description
                          </th>
                          <th scope="col" className="px-6 py-3">
                          Release Year
                          </th>
                          <th scope="col" className="px-6 py-3">
                          Size
                          </th>
                          <th scope="col" className="px-6 py-3">
                          Price
                          </th>
                          <th scope="col" className="px-6 py-3">
                          Rating
                          </th>
                          <th scope="col" className="px-6 py-3">
                          Platform
                          </th>
                          <th scope="col" className="px-6 py-3">
                          Action
                          </th>
                          <th scope="col" className="px-6 py-3">
                            <span className="sr-only">Edit</span>
                          </th>
                        </tr>
                      </thead>
                      <tbody>
                      {dataMahasiswa !== null&&( //kita membuat kondisi agar useState() berisi kosong agar bisa ke beckup
                        <>
                         {
                           dataMahasiswa.map((res,index)=>{ {/* map menampilakn hasil berupa list []  https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/map*/}
                                              
                            return(
                              <tr className="bg-white " key={res.id}> {/* sintak key sama seperti konsep id unik pada sql tapi kita mengunakan index dan agar tidak ada warning "Each child in a list should have a unique "key" prop." diconsole brouser */}
                                <th scope="row" className="px-6 py-4 font-medium text-gray-900  whitespace-nowrap">
                                {index+1}
                                </th>
                                <th scope="row" className="px-6 py-4 font-medium text-gray-900  whitespace-nowrap">
                                {res.name}
                                </th>    
                                <td className="px-6 py-4">{res.category}</td>
                                <td className="px-6 py-4">{res.description}</td>
                                <td className="px-6 py-4">{res.release_year}</td>
                                <td className="px-6 py-4">{res.size}</td>
                                <td className="px-6 py-4">{res.price}</td>
                                <td className="px-6 py-4">{res.rating}</td>
                                <td className="px-6 py-4">{handleIndexScore( res.is_ios_app)}</td>     
                                <td className="px-6 py-4 text-right">

                                <button onClick={handleEdit} value={res.id} className="text-dark bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-lg text-sm w-full sm:w-auto px-5 py-2.5 text-center dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800">Update</button>
                                <button onClick={handleDelete} value={res.id} class="focus:outline-none text-white bg-red-700 hover:bg-red-800 focus:ring-4 focus:ring-red-300 font-medium rounded-lg text-sm px-5 py-2.5 mr-2 mb-2 dark:bg-red-600 dark:hover:bg-red-700 dark:focus:ring-red-900">Delete</button>

                                </td>
                              </tr>
                                    )
                                 })
                              }
                            </>
                         )}

                      </tbody>
                    </table>
                  </div>
                  <p></p>
                  <div className='container-form'>
                      <form onSubmit={handleSubmit}>
                          <div className="mb-6">
                            <label htmlFor="text" className="block ">Name</label>
                            <input onChange={handleChange} value={input.name} type="text"name="name" className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500" placeholder="tembak tembakan" required />
                          </div>
                          <div className="mb-6">
                            <label htmlFor="text" className="block ">Category</label>
                            <input onChange={handleChange}value={input.category} type="text"name="category" className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500" placeholder="Horor" required />
                          </div>
                          <div className="mb-6">
                            <label htmlFor="text" className="block ">Description</label>
                            <input  onChange={handleChange}value={input.description} type="text"name="description" className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500" placeholder="Deskripsi" required />
                          </div>
                          <div className="mb-6">
                            <label htmlFor="text" className="block ">Release Year</label>
                            <input onChange={handleChange}value={input.release_year} type="number"name="release_year" className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500" placeholder="2000" required />
                          </div>
                          <div className="mb-6">
                            <label htmlFor="text" className="block ">Size</label>
                            <input onChange={handleChange}value={input.size} type="number"name="size" className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500" placeholder="1-10000000000 KB" required />
                          </div>
                          <div className="mb-6">
                            <label htmlFor="text" className="block ">Price</label>
                            <input onChange={handleChange}value={input.price} type="number"name="price" className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500" placeholder="1-100000000000 IDR" required />
                          </div>
                          <div className="mb-6">
                            <label htmlFor="text" className="block ">Rating</label>
                            <input onChange={handleChange}value={input.rating} type="number"name="rating" className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500" placeholder="1-5" required />
                          </div>
                          <div className="mb-6">
                            <label htmlFor="text" className="block ">url</label>
                            <input onChange={handleChange}value={input.image_url} type="text"name="image_url" className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500" placeholder="URL" required />
                          </div>
                          <div className="mb-6">
                            <label htmlFor="text" className="block ">Platform</label>
                            <input onChange={handleChange}value={input.is_ios_app} type="number"name="is_ios_app" className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500" placeholder="Platform 1 untuk androis" required />
                          </div>
                          <button type="submit" className="text-white bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-lg text-sm w-full sm:w-auto px-5 py-2.5 text-center dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800">Kirim</button>
                      </form>
                  </div>
                </div>
              </div>
            </div>
    </>
  );
}
export default Crudquiz;