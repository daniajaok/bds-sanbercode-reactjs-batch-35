import logo from './logo.svg';
import './App.css';
import Tugas10 from './Tugas10/Tugas10'
import Tugas11 from './Tugas11/Tugas11'
import Tugas12 from './Tugas12/Tugas12'
import Tugas13 from './Tugas13/Mahasiswa';
import Routes from './rauters/routes';

import { BrowserRouter as Router,Switch,Route,Link  } from 'react-router-dom';
import { AlertDanger } from './Context/Alert';


const App=()=>{

    return(
        <>
        {/* <button type='button' className='text-white bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:ring-blue-300 font-medium rounded-lg text-sm px-5 py-2.5 mr-2 mb-2 dark:bg-blue-600 dark:hover:bg-blue-700 focus:outline-none dark:focus:ring-blue-800'>
         chekkkk
        </button>
        <AlertDanger/> */}
       <Routes/>
        </>
    )
} 

export default App;
