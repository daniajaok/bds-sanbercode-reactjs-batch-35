import logo from '../logo.svg';
import '../App.css';
import React, { useEffect, useState } from 'react';
import './Tugas.css'
import axios from 'axios';


const Tugas12=()=> {

 /* 
  const [dataMahasiswa,setDataMahasiswa] kosep geter  dan seter, dataMahasiswa itu getter sedangakn yg kanannya setter.
  merubah value di state tersebut, kita panggilnya yg sebelah kanan
  */
  const [dataMahasiswa,setDataMahasiswa]=useState([])
  const [fetchStatus,setFetchStatus]=useState(true)//tips agara lebih mudah
  const [inputName, setInputName] =  useState("")//assign value 
  const [currentId, setCurrentId] =  useState(null)

  useEffect(()=>{
    let fetchData= async()=>{
        let {data} = await axios.get(`http://backendexample.sanbercloud.com/api/student-scores`)//kita akan mengunakan Destructuring adalah memecah komponen yang kompleks menjadi komponen yang lebih kecil dan mengunakan async promise
        //console.log(data)
        //setDataMahasiswa([...data])
        let result = data.map((res)=>{  
            //https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/map
            
            let {
                course,
                id,
                name,
                score,


            }=res//kita akan mengunakan Destructuring adalah memecah komponen yang kompleks menjadi komponen yang lebih kecil
            return{                
                course,
                id,
                name,
                score,}

            
        })
        setDataMahasiswa([...result])

    }
    if(fetchStatus){//trik if 
        fetchData()
        setFetchStatus(false)
    }
    
    
  },[fetchStatus,setFetchStatus])//dependensi https://www.youtube.com/watch?v=uXsVSorOtAY 1:16:25 / 1:38:12
  console.log(dataMahasiswa)
  const handleIndexScore=(prams)=>{
    if(prams>=80){
        return "A"
    }
    else if (prams >= 70 && prams<80) {
        
        return "B"
    } 
    else if (prams >= 60&& prams<70) {
        
        return "C"
    }
    else if (prams >= 50&& prams<60) {
        
        return "D"
    }
    else {
        
        return "e"
    }
    

  }
  /* 
  const [input,setInput] kosep geter  dan seter, daftarBuah itu getter sedangakn yg kanannya setter.
  merubah value di state tersebut, kita panggilnya yg sebelah kanan
  */
  const [input,setInput]=useState({
      name:"",
      course:"",
      score:0
      //tempat penapung get maupun post atau  geter  dan seter default
      ////assign value 


  })
  const handleChange=(event)=>{//event adalah parameter 
    console.log(event.target)
    //event.terget berfungsi nemapilan atribut <input type="text"name="name" value>
    console.log(event.target.name)//event.target.name untuk memilih target spesifik
    let nameOfInput=event.target.name
    let valueOfInput=event.target.value
    console.log(valueOfInput)
    //cara menetapkan secara langsung ke dalam useState dengan cara 
    setInput({...input,[nameOfInput]:valueOfInput})
  }
  //console.log(setInput)
  console.log("hasil dari input "+input)
  console.log(fetchStatus)
  //
  const handleSubmit=(event)=>{//event adalah parameter
    event.preventDefault()
    console.log("hasil format"+input)//chek apakah sudah sesuai format array of oject dataMahasiswa
    let {name,course,score}=input//kita akan mengunakan Destructuring adalah memecah komponen yang kompleks menjadi komponen yang lebih kecil
    console.log("ini adalah nama "+name)
    console.log("ini adalah id"+currentId)

    if (currentId === null){
      // untuk create data baru
      axios.post(`https://backendexample.sanbercloud.com/api/student-scores`, {name,course,score})
      .then(res => {
        console.log(res)//untuk menagkap error 
          setFetchStatus(true)//1:11:56 / 1:38:12 https://www.youtube.com/watch?v=uXsVSorOtAY
      //    let data = res.data
      //    setPesertaLomba([...pesertaLomba, {id: data.id, name: data.name}])
     })
    }
    else{
      axios.put(`https://backendexample.sanbercloud.com/api/student-scores/${currentId}`, {name,course,score})
      .then((res) => {
        console.log(res)//untuk menagkap error 
        setFetchStatus(true)//1:11:56 / 1:38:12 https://www.youtube.com/watch?v=uXsVSorOtAY
          //let singlePeserta = currentId.find(el=> el.id === currentId)
          //singlePeserta.name= input.name
          //console.log("data nama"+singlePeserta.name)
          //setDataMahasiswa([...dataMahasiswa])
      })
    }



    setInput({
        name:"",
        course:"",
        score:0
    })


  }



  const handleDelete=(event)=>{
  let idData = parseInt(event.target.value)
  console.log(idData)//cek apakah id udah terambil atau belum
  axios.delete(`https://backendexample.sanbercloud.com/api/student-scores/${idData}`)
  .then((res)=>{
    console.log(res)//untuk menagkap error 
    setFetchStatus(true)
    
  })
  .catch((err)=>{
    console.log(err)//untuk menagkap error

})

  }



  const handleEdit=(event)=>{
    let idData = parseInt(event.target.value)
    console.log("tampilkan edit ID"+idData)//cek apakah id udah terambil atau belum
    axios.get(`https://backendexample.sanbercloud.com/api/student-scores/${idData}`)
    .then(res => {
      let data = res.data

      setInput(data)//=>araay of object diamasukan setInput diluar fungsi
      //setDataMahasiswa(data.name,data.course,data.score)
      console.log(data)
      console.log("tampilkan edit semua data "+data.name,data.course,data.score)
      setCurrentId(data.id)
      console.log("tampilkan edit id "+data.id)
    })
  }


  

  return (
    <>
        <table>
          <tr>
          <th>No</th>
          <th>Nama</th>
          <th>Mata Kuliah</th>
          <th>Nilai</th>
          <th>Index Nilai</th>
          <th>aksi</th>
          </tr>

            {dataMahasiswa !== null&&( //kita membuat kondisi agar useState() berisi kosong agar bisa ke beckup
              <>
                {
                  dataMahasiswa.map((res,index)=>{ {/* map menampilakn hasil berupa list []  https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/map*/}
                  
                    return(
                      <tr key={res.id}> {/* sintak key sama seperti konsep id unik pada sql tapi kita mengunakan index dan agar tidak ada warning "Each child in a list should have a unique "key" prop." diconsole brouser */}
                        <td>{index+1}</td>
                        <td>{res.name}</td>
                        <td>{res.course}</td>
                        <td>{res.score}</td>
                        <td>{handleIndexScore( res.score)}</td>
                        
                        <td>
                          <button onClick={handleEdit} value={res.id}>edit</button>
                          <button onClick={handleDelete} value={res.id}>delete</button>
                        </td>
                      </tr>
                    )
                  })
                }
              </>
            )}
      </table>
      <p></p>
      <div className='container-form'>
          <form onSubmit={handleSubmit}>{/* handleSubmit adalah methot untuk mengolah data dari input(yang telah sesui format aray of objeck) ke dalam setDataBuah atau setter*/}
          <label >Name</label>
          <input onChange={handleChange} value={input.name} type="text"name="name"/>
          <label >Mata Kuliah</label>
          <input onChange={handleChange}value={input.course} type="text"name="course" />
          <label >Nilai</label>
          <input onChange={handleChange}value={input.score} type="number"name="score" />
          {/* onChange={handleChange} onChange berfungsi untuk merubah nilai value pada html sedangkan handleChange ada fugsi hendel nilai input*/}
          <input type="submit" value="Submit"/>
          </form>
        </div>

    </>
  );
}

export default Tugas12