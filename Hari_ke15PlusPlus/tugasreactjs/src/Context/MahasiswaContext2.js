import React,{createContext, useState} from "react";
import axios from 'axios';
import { useHistory } from "react-router-dom";
//
export const MahasiswaContext2=createContext()
export const MahasiswaProvider2=props=>{

    let history = useHistory()
    const [dataMahasiswa,setDataMahasiswa]=useState([])
    const [fetchStatus,setFetchStatus]=useState(true)//tips agara lebih mudah
    const [currentId, setCurrentId] =  useState(null)

    const[tes]=useState("haasaasasasasasasassalo")
    const [input,setInput]=useState({
        name:"",
        course:"",
        score:0
        //tempat penapung get maupun post atau  geter  dan seter default
        ////assign value 
  
  
    })
    const handleIndexScore=(prams)=>{
        if(prams>=80){
            return "A"
        }
        else if (prams >= 70 && prams<80) {
            
            return "B"
        } 
        else if (prams >= 60&& prams<70) {
            
            return "C"
        }
        else if (prams >= 50&& prams<60) {
            
            return "D"
        }
        else {
            
            return "e"
        }
        
    
      }
      const handleChange=(event)=>{//event adalah parameter 
        console.log(event.target)
        //event.terget berfungsi nemapilan atribut <input type="text"name="name" value>
        console.log(event.target.name)//event.target.name untuk memilih target spesifik
        let nameOfInput=event.target.name
        let valueOfInput=event.target.value
        console.log(valueOfInput)
        //cara menetapkan secara langsung ke dalam useState dengan cara 
        setInput({...input,[nameOfInput]:valueOfInput})
      }


      const handleSubmit=(event)=>{//event adalah parameter
        event.preventDefault()
        console.log("hasil format"+input)//chek apakah sudah sesuai format array of oject dataMahasiswa
        let {name,course,score}=input//kita akan mengunakan Destructuring adalah memecah komponen yang kompleks menjadi komponen yang lebih kecil
        console.log("ini adalah nama "+name)
        console.log("ini adalah id"+currentId)
    
        if (currentId === null){
          // untuk create data baru
          axios.post(`https://backendexample.sanbercloud.com/api/student-scores`, {name,course,score})
          .then(res => {
            console.log(res)//untuk menagkap error 
            setFetchStatus(true)//1:11:56 / 1:38:12 https://www.youtube.com/watch?v=uXsVSorOtAY
          //    let data = res.data
          //    setPesertaLomba([...pesertaLomba, {id: data.id, name: data.name}])
            history.push('/Tugas14')// bila sudah mnegisi maka akan pindah ke url /Tugas14
         })
        }
        else{
          axios.put(`https://backendexample.sanbercloud.com/api/student-scores/${currentId}`, {name,course,score})
          .then((res) => {
            console.log(res)//untuk menagkap error 
            setFetchStatus(true)//1:11:56 / 1:38:12 https://www.youtube.com/watch?v=uXsVSorOtAY
              //let singlePeserta = currentId.find(el=> el.id === currentId)
              //singlePeserta.name= input.name
              //console.log("data nama"+singlePeserta.name)
              //setDataMahasiswa([...dataMahasiswa])
              history.push('/Tugas14')
          })
        }
    
    
    
        setInput({
            name:"",
            course:"",
            score:0
        })
      }
      const handleEdit=(event)=>{
        let idData = parseInt(event.target.value)
        console.log("tampilkan edit ID"+idData)//cek apakah id udah terambil atau belum
        axios.get(`https://backendexample.sanbercloud.com/api/student-scores/${idData}`)
        .then(res => {
          let data = res.data
    
          setInput(data)//=>araay of object diamasukan setInput diluar fungsi
          //setDataMahasiswa(data.name,data.course,data.score)
          console.log(data)
          console.log("tampilkan edit semua data "+data.name,data.course,data.score)
          setCurrentId(data.id)
          console.log("tampilkan edit id "+data.id)
        })
        history.push(`/Tugas14/edit/${idData}`)
      }//


    
    const handleDelete=(event)=>{
        let idData = parseInt(event.target.value)
        console.log(idData)//cek apakah id udah terambil atau belum
        axios.delete(`https://backendexample.sanbercloud.com/api/student-scores/${idData}`)
        .then((res)=>{
          console.log(res)//untuk menagkap error 
          setFetchStatus(true)
          
        })
        .catch((err)=>{
          console.log(err)//untuk menagkap error
      
      })
      
    }
      

    


    

    ///////inisialisasi fungsi dan parameter//// 
    let state={
        dataMahasiswa,setDataMahasiswa,
        fetchStatus,setFetchStatus,
        currentId, setCurrentId,
        input,setInput

    }

    let FhandleIndexScore={
        handleIndexScore
    
    }
    let FhandleChange={
        handleChange
    }
    let FhandleSubmit={

        handleSubmit
    }
    let FhandleEdit={
        handleEdit
    }
    let FhandleDelete={
        handleDelete
    }



    return(
        
        <MahasiswaContext2.Provider value={{
            state,FhandleIndexScore,
            FhandleChange,FhandleSubmit,
            FhandleEdit,FhandleDelete
            }}>
            {props.children}{/*MahasiswaContext.Provider value menampung semua state atau hooks lalu dibuat
            {props.children} untuk menampung semua props lalu ditampung lagi kepada Mahasiswa.js */}
        </MahasiswaContext2.Provider>
    )
}