import React from 'react';
import Tugas10 from '../Tugas10/Tugas10';
import Tugas11 from '../Tugas11/Tugas11';
import Tugas12 from '../Tugas12/Tugas12';
import Mahasiswa from '../Tugas13/Mahasiswa'
import MahasiswaList from '../Tugas14/MahasiswaList'
import MahasiswaList3 from '../Tugas15/MahasiswaList';
import MahasiswaFrom from "../Tugas14/MahasiswaFrom";
import { MahasiswaProvider } from "../Context/MahasiswaContext";
import {  MahasiswaProvider2} from '../Context/MahasiswaContext2';
import { BrowserRouter as Router,Switch,Route,Link  } from 'react-router-dom';
import Nav from '../Tugas14/nav'
const routes=(props)=> {
    return (
        <>
        <Router>
            <Nav/>
            <MahasiswaProvider2>
                    <Switch>
                    <Route path="/Tugas15" exact component={MahasiswaList3}/>
                    <Route path="/Tugas14" exact component={MahasiswaList}/>
                    <Route path="/Tugas14/create" exact component={MahasiswaFrom}/>
                    <Route path="/Tugas14/edit/:Id" exact component={MahasiswaFrom}/>
                    </Switch>
            </MahasiswaProvider2>
            
                <Switch>
                    <MahasiswaProvider>
                    <Route path="/Tugas10" exact component={Tugas10}/>
                    <Route path="/Tugas11" exact component={Tugas11}/>
                    <Route path="/Tugas12" exact component={Tugas12}/>
                    <Route path="/Tugas13" exact component={Mahasiswa}/>
                    </MahasiswaProvider>

                </Switch>
            
        </Router>
        </>
    );
}
//18:36

export default routes;