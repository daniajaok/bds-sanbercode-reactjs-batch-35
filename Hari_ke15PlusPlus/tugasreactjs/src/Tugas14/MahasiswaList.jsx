import axios from 'axios';
import './Tugas.css'
import React, { useContext, useEffect, useState } from 'react';
import { MahasiswaContext2 } from '../Context/MahasiswaContext2';
import { Link, useParams } from 'react-router-dom';
//
const MahasiswaList=()=>{
    const{state}=useContext(MahasiswaContext2)
    let {

        dataMahasiswa,setDataMahasiswa,
        fetchStatus,setFetchStatus,
        currentId, setCurrentId,
        input,setInput
    }=state

    const{FhandleIndexScore}=useContext(MahasiswaContext2)
    let {

        handleIndexScore
    }=FhandleIndexScore
    const{FhandleEdit}=useContext(MahasiswaContext2)
    let {

        handleEdit
    }=FhandleEdit
    const{FhandleDelete}=useContext(MahasiswaContext2)
    let {

        handleDelete
    }=FhandleDelete
    ///
    const{FhandleChange}=useContext(MahasiswaContext2)
    let {

        handleChange
    }=FhandleChange
    const{FhandleSubmit}=useContext(MahasiswaContext2)
    let {

        handleSubmit
    }=FhandleSubmit

/* 
  const [dataMahasiswa,setDataMahasiswa] kosep geter  dan seter, dataMahasiswa itu getter sedangakn yg kanannya setter.
  merubah value di state tersebut, kita panggilnya yg sebelah kanan
  */

  useEffect(()=>{
    let fetchData= async()=>{
        let {data} = await axios.get(`http://backendexample.sanbercloud.com/api/student-scores`)//kita akan mengunakan Destructuring adalah memecah komponen yang kompleks menjadi komponen yang lebih kecil dan mengunakan async promise
        //console.log(data)
        //setDataMahasiswa([...data])
        let result = data.map((res)=>{  
            //https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/map
            
            let {
                course,
                id,
                name,
                score,


            }=res//kita akan mengunakan Destructuring adalah memecah komponen yang kompleks menjadi komponen yang lebih kecil
            return{                
                course,
                id,
                name,
                score,}

            
        })
        setDataMahasiswa([...result])

    }
    if(fetchStatus){//trik if 
        fetchData()
        setFetchStatus(false)
    }
    
    
  },[fetchStatus,setFetchStatus])//dependensi https://www.youtube.com/watch?v=uXsVSorOtAY 1:16:25 / 1:38:12
  console.log(dataMahasiswa)

  /* 
  const [input,setInput] kosep geter  dan seter, daftarBuah itu getter sedangakn yg kanannya setter.
  merubah value di state tersebut, kita panggilnya yg sebelah kanan
  */


  //console.log(setInput)
  console.log("hasil dari input "+input)
  console.log(fetchStatus)
  //











  

  return (
    <>

        <table>
          <tr>
          <th>No</th>
          <th>Nama</th>
          <th>Mata Kuliah</th>
          <th>Nilai</th>
          <th>Index Nilai</th>
          <th>aksi</th>
          </tr>

            {dataMahasiswa !== null&&( //kita membuat kondisi agar useState() berisi kosong agar bisa ke beckup
              <>
                {
                  dataMahasiswa.map((res,index)=>{ {/* map menampilakn hasil berupa list []  https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/map*/}
                  
                    return(
                      <tr key={res.id}> {/* sintak key sama seperti konsep id unik pada sql tapi kita mengunakan index dan agar tidak ada warning "Each child in a list should have a unique "key" prop." diconsole brouser */}
                        <td>{index+1}</td>
                        <td>{res.name}</td>
                        <td>{res.course}</td>
                        <td>{res.score}</td>
                        <td>{handleIndexScore( res.score)}</td>
                        
                        <td>
                          <button onClick={handleEdit} value={res.id}>edit</button>
                          <button onClick={handleDelete} value={res.id}>delete</button>
                        </td>
                      </tr>
                    )
                  })
                }
              </>
            )}
      </table>
      <p></p>
      <Link to={'/Tugas14/create'}><button>create data</button></Link>


    </>
  );
}
export default MahasiswaList