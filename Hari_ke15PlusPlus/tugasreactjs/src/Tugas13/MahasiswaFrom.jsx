import logo from '../logo.svg';
import '../App.css';
import React, { useEffect, useState,useContext } from 'react';
import './Tugas.css'
import axios from 'axios';
import { MahasiswaContext } from '../Context/MahasiswaContext';


const MahasiswaFrom=()=> {

    const{state}=useContext(MahasiswaContext)
    let {

        dataMahasiswa,setDataMahasiswa,
        fetchStatus,setFetchStatus,
        currentId, setCurrentId,
        input,setInput
    }=state
    const{FhandleChange}=useContext(MahasiswaContext)
    let {

        handleChange
    }=FhandleChange
    const{FhandleSubmit}=useContext(MahasiswaContext)
    let {

        handleSubmit
    }=FhandleSubmit

    ////
    const{FhandleIndexScore}=useContext(MahasiswaContext)
    let {

        handleIndexScore
    }=FhandleIndexScore
    const{FhandleEdit}=useContext(MahasiswaContext)
    let {

        handleEdit
    }=FhandleEdit
    const{FhandleDelete}=useContext(MahasiswaContext)
    let {

        handleDelete
    }=FhandleDelete




    
  useEffect(()=>{
    let fetchData= async()=>{
        let {data} = await axios.get(`http://backendexample.sanbercloud.com/api/student-scores`)//kita akan mengunakan Destructuring adalah memecah komponen yang kompleks menjadi komponen yang lebih kecil dan mengunakan async promise
        //console.log(data)
        //setDataMahasiswa([...data])
        let result = data.map((res)=>{  
            //https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/map
            
            let {
                course,
                id,
                name,
                score,


            }=res//kita akan mengunakan Destructuring adalah memecah komponen yang kompleks menjadi komponen yang lebih kecil
            return{                
                course,
                id,
                name,
                score,}

            
        })
        setDataMahasiswa([...result])

    }
    if(fetchStatus){//trik if 
        fetchData()
        setFetchStatus(false)
    }
    
    
  },[fetchStatus,setFetchStatus])//dependensi https://www.youtube.com/watch?v=uXsVSorOtAY 1:16:25 / 1:38:12
  console.log(dataMahasiswa)
  
  /* 
  const [input,setInput] kosep geter  dan seter, daftarBuah itu getter sedangakn yg kanannya setter.
  merubah value di state tersebut, kita panggilnya yg sebelah kanan
  */


  //console.log(setInput)
  console.log("hasil dari input "+input)
  console.log(fetchStatus)
  //






  

  return (
    <>
        
      <div className='container-form'>
          <form onSubmit={handleSubmit}>{/* handleSubmit adalah methot untuk mengolah data dari input(yang telah sesui format aray of objeck) ke dalam setDataBuah atau setter*/}
          <label >Name</label>
          <input onChange={handleChange} value={input.name} type="text"name="name"/>
          <label >Mata Kuliah</label>
          <input onChange={handleChange}value={input.course} type="text"name="course" />
          <label >Nilai</label>
          <input onChange={handleChange}value={input.score} type="number"name="score" />
          {/* onChange={handleChange} onChange berfungsi untuk merubah nilai value pada html sedangkan handleChange ada fugsi hendel nilai input*/}
          <input type="submit" value="Submit"/>
          </form>
        </div>

    </>
  );
}
//
export default MahasiswaFrom