import logo from '../logo.svg';
import '../App.css';
import React, { useState } from 'react';
import './Tugas.css'


const Tugas11=()=> {
   
  /* 
  const [daftarBuah ,setDataBuah] kosep geter  dan seter, daftarBuah itu getter sedangakn yg kanannya setter.
  merubah value di state tersebut, kita panggilnya yg sebelah kanan
  */
  const [daftarBuah ,setDataBuah]=useState([
    {nama: "Nanas", hargaTotal: 100000, beratTotal: 4000 },
    {nama: "Manggis", hargaTotal: 350000, beratTotal: 10000},
    {nama: "Nangka", hargaTotal: 90000, beratTotal: 2000},
    {nama: "Durian", hargaTotal: 400000, beratTotal: 5000},
    {nama: "Strawberry", hargaTotal: 120000, beratTotal: 6000}
  ])
  var hasilmap=daftarBuah.map(variable=>variable.nama)//https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/map
  //variable=>variable sebuah inisialisai
  console.log(hasilmap)

  /* 
  const [input,setInput] kosep geter  dan seter, daftarBuah itu getter sedangakn yg kanannya setter.
  merubah value di state tersebut, kita panggilnya yg sebelah kanan
  */
  const [input,setInput]=useState({
      nama:"",
      hargaTotal:0,
      beratTotal:0
      //tempat penapung get maupun post atau  geter  dan seter default


  })
  const [currentIndex, setCurrentIndex] = useState(-1)

  const handleChange=(event)=>{//event adalah parameter 
    console.log(event.target)
    //event.terget berfungsi nemapilan atribut <input type="text"name="nama" value>
    console.log(event.target.name)//event.target.name untuk memilih target spesifik
    let nameOfInput=event.target.name
    let valueOfInput=event.target.value
    console.log(valueOfInput)
    //if(nameOfInput==="hargaTotal"){
    //  setInput({...input,hargaTotal:valueOfInput})
    //}//1:03:30 / 1:29:26
    //cara menetapkan secara langsung ke dalam useState dengan cara 
    setInput({...input,[nameOfInput]:valueOfInput})

  }
  //console.log(setInput)
  console.log(input)

  const handleSubmit=(event)=>{//event adalah parameter
    //event.preventDefault()
    //console.log(input)//chek apakah sudah sesuai format array of oject daftarBuah
    //kita akan mengunakan Destructuring adalah memecah komponen yang kompleks menjadi komponen yang lebih kecil
    //let {nama,hargaTotal,beratTotal}=input
    //console.log("ini adalah nama "+nama)
    //let newData=daftarBuah
    //newData=[...daftarBuah,{nama:input.nama, hargaTotal:input.hargaTotal, beratTotal:input.beratTotal }]// <=ini mengukan cara manual
    // ini mengunakan  Enhanced object literals 
    //newData=[...daftarBuah,{nama,hargaTotal,beratTotal}]
    //console.log(...newData+"data")
    //setDataBuah([...newData])
    // di bawah ini bisa menghendel edit dan Submit
    event.preventDefault()
    let newData = daftarBuah
    let {nama,hargaTotal,beratTotal}=input

   if (currentIndex === -1) {
     newData = [...daftarBuah, {nama,hargaTotal,beratTotal}]
   } else {
     newData[currentIndex] = {nama,hargaTotal,beratTotal}
    }

    setDataBuah(newData)
    setInput("")


  }


  const HhandleSubmit = (event) => { //sama seperti handleSubmit atas
    event.preventDefault() 
     
    let {nama, hargaTotal, beratTotal} = input 
    let newData = daftarBuah 
     
    // setDisplay(true) 

    // setTimeout(()=> { 
        // if(currentIndex === -1){ 
            newData = [...daftarBuah, {nama, hargaTotal, beratTotal}] 
        // }else { 
            // newData[currentIndex] = { nama, hargaTotal, beratTotal } 
        // } 
        //setDisplay(false) 
        setDataBuah([...newData]) 
    // }, 3000) 

    setInput({ 
        nama: "", 
        hargaTotal: 0, 
        beratTotal: 0 
    }) 
  }
  const handleDelete=(event)=>{
  let index = parseInt(event.target.value)
  let deletedItem = daftarBuah[index]
  let newData = daftarBuah.filter((e) => {return e !== deletedItem})
  console.log(newData)
  setDataBuah(newData)
  }

  const handleEdit=(event)=>{
    let index = parseInt(event.target.value)
    let editValue = daftarBuah[index]
    setInput(editValue)
    setCurrentIndex(event.target.value)
  }

  return (
    <>
        <table>
          <tr>
          <th>No</th>
          <th>Nama</th>
          <th>Harga Total</th>
          <th>Berat Total</th>
          <th>Harga per Kg</th>
          <th>aksi</th>
          </tr>

            {daftarBuah !== null&&( //kita membuat kondisi agar useState() berisi kosong agar bisa ke beckup
              <>
                {
                  daftarBuah.map((e,index)=>{ {/* map menampilakn hasil berupa list [] bukan sting contoh seperti di  varibale hasilmap tau line 16 */}
                  
                    return(
                      <tr key={index}> {/* sintak key sama seperti konsep id unik pada sql tapi kita mengunakan index dan agar tidak ada warning "Each child in a list should have a unique "key" prop." diconsole brouser */}
                        <td>{index+1}</td>
                        <td>{e.nama}</td>
                        <td>{e.hargaTotal}</td>
                        <td>{e.beratTotal}</td>
                        <td>{e.hargaTotal/(e.beratTotal/100)}kg</td>
                        <td>
                          <button onClick={handleEdit} value={index}>edit</button>
                          <button onClick={handleDelete} value={index}>delete</button>
                        </td>
                      </tr>
                    )
                  })
                }
              </>
            )}
      </table>
      <p></p>
      <div className='container-form'>
          <form onSubmit={handleSubmit}>{/* handleSubmit adalah methot untuk mengolah data dari input(yang telah sesui format aray of objeck) ke dalam setDataBuah atau setter*/}
          <label >Name</label>
          <input onChange={handleChange} value={input.nama} type="text"name="nama"/>
          <label >Harga Total</label>
          <input onChange={handleChange}value={input.hargaTotal} type="number"name="hargaTotal" />
          <label >Berat Total</label>
          <input onChange={handleChange}value={input.beratTotal} type="number"name="beratTotal" />
          {/* onChange={handleChange} onChange berfungsi untuk merubah nilai value pada html sedangkan handleChange ada fugsi hendel nilai input*/}
          <input type="submit" value="Submit"/>
          </form>
        </div>

    </>
  );
}

export default Tugas11